# Compte Rendu #

### Configuration ###

* Cloner la branche master
* Lancer mvn tomcat7:run
* API disponible à l'adresse http://localhost:8080/rest

L'API permet de retourner, d'ajouter, de modifier et de supprimer des personnes, maisons, chauffages et appareils electrique.

# Classes #

## Person (src\main\java\domain\Person.java)##
Une personne possède un id, un nom, un prenom, une adresse mail, zero ou plusieurs maisons et zero ou plusieurs amis.

## House (src\main\java\domain\House.java)##
Une maison possède un id, une surface, un nombre de chambres, un et un seul propriétaire(Personne), zero ou plusieurs chauffages et zero ou plusieurs appareils electrique.
La classe a également une requete nommée 'House.all' permettant d'afficher toutes les maisons.

## Heater (src\main\java\domain\Heater.java)##
Hérite de la classe IntelligentDevice.
Un chauffage possède un id, une puissance et la maison dans laquelle il se situe.

## ElectronicDevice (src\main\java\domain\ElectronicDevice.java)##
Hérite de la classe IntelligentDevice.
Un appareil possède un id, une puissance et la maison dans laquelle il se situe.

## JpaTest (src\main\java\domain\JpaTest.java)##
Classe effectuant les interactions avec la base de données (API).
Les requêtes sont faite avec l'aide de Criteria.

### Methods ###

* createPerson: 
Ajoute une personne dans la base de données. Prend en paramètre un nom, un prenom, une adresse mail, une liste de maison et une liste d'amis.

* createHouse: 
Ajoute une maison dans la base de données. Prend en paramètre une surface, un nombre de chambres, l'id du propriétaire, une liste de chauffages et une liste d'appareils.

* createHeater: 
Ajoute un chauffage dans la base de données. Prend en paramètre une puissance et l'id de sa maison.

* createElectronicDevice: 
Ajoute un appareil dans la base de données. Prend en paramètre une puissance et l'id de sa maison.

* alterPerson: 
Modifie les informations d'une personne. Prends en paramètre l'id de la personne a modifier, le nouveau nom, le nouveau prenom, la nouvelle adresse mail, liste de maisons et liste d'amis.

* alterHouse:
Modifie les informations d'une maison. Prends en paramètre l'id de la maison à modifier, la nouvelle surface, le nouveau nombre de chambres, l'id du nouveau propriétaire, la nouvelle liste de chauffages et liste d'appareils.

* alterHeater: 
Modifie les informations d'un chauffage. Prends en paramètre l'id du chauffage à modifier, la nouvelle puissance et l'id de la nouvelle maison.

* alterDevice: 
Modifie les informations d'un apareil electrique. Prends en paramètre l'id de l'appareil à modifier, la nouvelle puissance et l'id de la nouvelle maison.

* getPersonById / getHouseById / getHeaterById / getDeviceById: 
Retourne la personne, la maison, le chauffage ou l'appareil possèdant l'id passé en paramètre.

* deletePerson / deleteHouse / deleteHeater / deleteDevice: 
Supprime la personne, la maison, le chauffage ou l'appareil possèdant l'id passé en paramètre.

* showPersons / showHouses / showHeaters / showDevices: 
Retourne une liste d'objet typée contenant toutes les personnes, maisons, chauffages ou appareil existant dans la base.

* houseAddHeater: 
Ajoute un chauffage à une maison. Prends en paramètre l'id de la maison et l'id du chauffage.

* houseAddDevice:
Ajoute un appareil à une maison. Prends en paramètre l'id de la maison et l'id du chauffage.

* personAddHouse:
Ajoute une maison à une personne. Prends en paramètre l'id de la personne et l'id de la maison.

* personAddFriend:
Ajoute un ami à une personne. Prends en paramètre l'id de la personne et l'id de l'ami.
L'ajout se fait dans les deux sens, la personne qui ajoute un ami se voit rajouter dans la liste d'amis de l'ami.

### Classes ###
## Person ('\src\main\java\fr\istic\sir\rest\Person.java') ##
Définit les routes: 

- person : retourne la liste de toutes les personnes avec leurs infos.

- person/new-person : formulaire pour ajouter une personne (plus utiliser avec le TP6).

- person/{id} : affiche les infos de la personnes avec l'id '{id}'.Permet de modifier les informations de cette personnes. (plus utiliser avec le TP6)

- person/delete-user/{id} : supprime la personne possèdant l'id '{id}'.

Les routes person/create-person et person/update-person sont appelées par les formulaires d'ajout et de modification afin de traiter les infos des formulaires et les utiliser avec les fonctions de la classe JpaTest.

- Les classes House, Heater et ElectronicDevice définissent les mêmes routes pour leur propre classe(remplacer person par house, heater ou device).

## Index ('\src\main\java\fr\istic\sir\rest\Index.java') ##
Renvoi une vue pour la route 'rest/' (plus utiliser avec le TP6)

## MyApplication ('\src\main\java\fr\istic\sir\rest\MyApplication.java') ##
Classe permettant d'initialiser l'utilisation de jersey-mvc(template) (plus utiliser avec le TP6)