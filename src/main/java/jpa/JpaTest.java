package jpa;

import domain.ElectronicDevice;
import domain.Heater;
import domain.House;
import domain.Person;

import javax.persistence.*;
import javax.persistence.criteria.*;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

public class JpaTest {

	private EntityManager manager;
	private CriteriaBuilder criteriaBuilder;
	private EntityTransaction tx;

	public EntityTransaction getTx() {
		return tx;
	}

	public void setTx(EntityTransaction tx) {
		this.tx = tx;
	}

	public CriteriaBuilder getCriteriaBuilder() {
		return criteriaBuilder;
	}

	public void setCriteriaBuilder(CriteriaBuilder criteriaBuilder) {
		this.criteriaBuilder = criteriaBuilder;
	}

	public EntityManager getManager() {
		return manager;
	}

	public void setManager(EntityManager manager) {
		this.manager = manager;
	}

	public JpaTest(EntityManager manager, CriteriaBuilder criteriaBuilder, EntityTransaction tx) {
		this.setManager(manager);
		this.setCriteriaBuilder(criteriaBuilder);
		this.setTx(tx);
	}
	public JpaTest(){
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("mysql");
		EntityManager manager = factory.createEntityManager();
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		EntityTransaction tx = manager.getTransaction();
		this.setManager(manager);
		this.setCriteriaBuilder(criteriaBuilder);
		this.setTx(tx);
	}

	public void createPersonb(String name, String firstName, String mail){
		this.getTx().begin();
		Person p = new Person(name, firstName, mail);
		this.getManager().persist(p);
		this.getTx().commit();
	}

	public void createPerson(String name, String firstName, String mail, List<Integer> houses, List<Integer> friends){
		this.getTx().begin();
		Person p = new Person(name, firstName, mail);
		this.getManager().persist(p);
		this.getTx().commit();

		for (Integer i: houses) {
			this.personAddHouse(p.getId(), Long.valueOf(i.longValue()) );
		}
		for (Integer i: friends) {
			this.personAddFriend(p.getId(), Long.valueOf(i.longValue()) );
		}
	}

	public void createHouse(int size, int rooms, long personId, List<Long> heaters, List<Long> devices){
		List<Heater> heaterList = new ArrayList<Heater>();
		List<ElectronicDevice> deviceList = new ArrayList<ElectronicDevice>();
		boolean b = false;
		Person p = null;

		if( personId != 0){
			p = this.getPersonById(personId);
		}else{
			b = true;
		}
		this.getTx().begin();
		House h;
		if(!b){
			h = new House(size, rooms, p);
		}else{
			h = new House(size, rooms);
		}
		this.getManager().persist(h);
		this.getTx().commit();

		this.getTx().begin();
		for (Long i: heaters) {
			if(i != 0)
				this.houseAddHeater(h.getId(), i );
		}
		for (Long i: devices) {
			if(i != 0)
				this.houseAddDevice(h.getId(), i );
		}
		this.getTx().commit();
	}

	public void createHeater(int power, long houseId){
		this.getTx().begin();
		House house = this.getHouseById(houseId);
		Heater h = new Heater(power, house);
		this.getManager().persist(h);
		this.getTx().commit();
	}
	public void createElectronicDevice(int power, long houseId){
		this.getTx().begin();
		House house = this.getHouseById(houseId);
		ElectronicDevice d = new ElectronicDevice(power, house);
		this.getManager().persist(d);
		this.getTx().commit();
	}

	public void alterHeater(long id, int power, long h){
		this.getTx().begin();
		House house = this.getHouseById(h);
		CriteriaUpdate update = this.getCriteriaBuilder().createCriteriaUpdate(Heater.class);
		Root heater = update.from(Heater.class);
		update.set("power", power);
		update.set("house", house);
		update.where(this.getCriteriaBuilder().equal(heater.<Long>get("id"), this.getCriteriaBuilder().parameter(Heater.class, "id")));
		Query query = this.getManager().createQuery(update);
		query.setParameter("id", id);
		query.executeUpdate();
		this.getTx().commit();
	}

	public void alterDevice(long id, int power, long h){
		this.getTx().begin();
		House house = this.getHouseById(h);
		CriteriaUpdate update = this.getCriteriaBuilder().createCriteriaUpdate(ElectronicDevice.class);
		Root device = update.from(ElectronicDevice.class);
		update.set("power", power);
		update.set("house", house);
		update.where(this.getCriteriaBuilder().equal(device.<Long>get("id"), this.getCriteriaBuilder().parameter(ElectronicDevice.class, "id")));
		Query query = this.getManager().createQuery(update);
		query.setParameter("id", id);
		query.executeUpdate();
		this.getTx().commit();
	}

	public void alterHouse(int houseId, int size, int rooms, int personId, List<Integer> heaters, List<Integer> devices ){
		Long l = new Long(houseId);
		Long l2 = new Long(personId);
		this.getTx().begin();
		House h = this.getHouseById(l);
		Person p = this.getPersonById(l2);

		CriteriaUpdate update = this.getCriteriaBuilder().createCriteriaUpdate(House.class);
		Root house = update.from(House.class);
		update.set("size", size);
		update.set("rooms", rooms);
		update.set("person", p);
		update.where(this.getCriteriaBuilder().equal(house.<Long>get("id"), this.getCriteriaBuilder().parameter(House.class, "id")));
		Query query = this.getManager().createQuery(update);
		query.setParameter("id", l);
		query.executeUpdate();

		CriteriaUpdate update2 = this.getCriteriaBuilder().createCriteriaUpdate(Heater.class);
		Root heater = update2.from(Heater.class);
		update2.set("house", null);
		update2.where(this.getCriteriaBuilder().equal(heater.<Long>get("house"), this.getCriteriaBuilder().parameter(House.class, "house")));
		Query query2 = this.getManager().createQuery(update2);
		query2.setParameter("house", h);
		query2.executeUpdate();

		h.getMyHeaters().clear();
		CriteriaUpdate update3 = this.getCriteriaBuilder().createCriteriaUpdate(ElectronicDevice.class);
		Root device = update3.from(ElectronicDevice.class);
		update3.set("house", null);
		update3.where(this.getCriteriaBuilder().equal(device.<Long>get("house"), this.getCriteriaBuilder().parameter(House.class, "house")));
		Query query3 = this.getManager().createQuery(update3);
		query3.setParameter("house", h);
		query3.executeUpdate();

		h.getMyDevices().clear();

		for (Integer he : heaters) {
			this.houseAddHeater(h.getId(), Long.valueOf(he.longValue()));
		}
		for (Integer d : devices) {
			this.houseAddDevice(h.getId(), Long.valueOf(d.longValue()));
		}

		this.getTx().commit();

	}

	public void alterPerson(int id, String name, String firstName, String mail, List<Integer> houses, List<Integer> friends){
		Long l = new Long(id);
		this.getTx().begin();
		Person p = this.getPersonById(l);

		CriteriaUpdate update = this.getCriteriaBuilder().createCriteriaUpdate(Person.class);
		Root person = update.from(Person.class);
		update.set("name", name);
		update.set("firstName", firstName);
		update.set("mail", mail);
		update.where(this.getCriteriaBuilder().equal(person.<Long>get("id"), this.getCriteriaBuilder().parameter(Person.class, "id")));
		Query query = this.getManager().createQuery(update);
		query.setParameter("id", l);
		query.executeUpdate();

		CriteriaUpdate update2 = this.getCriteriaBuilder().createCriteriaUpdate(House.class);
		Root house = update2.from(House.class);
		update2.set("person", null);
		update2.where(this.getCriteriaBuilder().equal(house.<Long>get("person"), this.getCriteriaBuilder().parameter(Person.class, "person")));
		Query query2 = this.getManager().createQuery(update2);
		query2.setParameter("person", p);
		query2.executeUpdate();

		p.getMyHouses().clear();

		for(Person fp : p.getMyFriends()){
			Iterator<Person> it = fp.getMyFriends().iterator();
			while(it.hasNext()){
				if(it.next().getId() == p.getId()) it.remove();
			}
		}

		p.getMyFriends().clear();

		this.getTx().commit();

		for (Integer h : houses) {
			this.personAddHouse(p.getId(), Long.valueOf(h.longValue()));
		}
		for (Integer f : friends) {
			this.personAddFriend(p.getId(), Long.valueOf(f.longValue()));
		}


	}

	public Person getPersonById(long personId){
		CriteriaQuery criteriaQuery = this.getCriteriaBuilder().createQuery();
		Root person = criteriaQuery.from(Person.class);
		criteriaQuery.select(person)
				.where(this.getCriteriaBuilder().equal(person.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query = this.getManager().createQuery(criteriaQuery);
		query.setParameter("id", personId);
		Person res = (Person) query.getSingleResult();
		return res;
	}

	public House getHouseById(long houseId){
		CriteriaQuery criteriaQuery = this.getCriteriaBuilder().createQuery();
		Root house = criteriaQuery.from(House.class);
		criteriaQuery.select(house)
				.where(this.getCriteriaBuilder().equal(house.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query = this.getManager().createQuery(criteriaQuery);
		query.setParameter("id", houseId);
		House res = (House) query.getSingleResult();
		return res;
	}

	public Heater getHeaterById(long heaterId){
		CriteriaQuery criteriaQuery = this.getCriteriaBuilder().createQuery();
		Root heater = criteriaQuery.from(Heater.class);
		criteriaQuery.select(heater)
				.where(this.getCriteriaBuilder().equal(heater.get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query = this.getManager().createQuery(criteriaQuery);
		query.setParameter("id", heaterId);
		Heater res = (Heater) query.getSingleResult();
		return res;
	}

	public ElectronicDevice getDeviceById(long deviceId){
		CriteriaQuery criteriaQuery = this.getCriteriaBuilder().createQuery();
		Root device = criteriaQuery.from(ElectronicDevice.class);
		criteriaQuery.select(device)
				.where(this.getCriteriaBuilder().equal(device.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query = this.getManager().createQuery(criteriaQuery);
		query.setParameter("id", deviceId);
		ElectronicDevice res = (ElectronicDevice) query.getSingleResult();
		return res;
	}


	public void deletePerson(long personId){

		this.getTx().begin();
		CriteriaQuery criteriaQuery = this.getCriteriaBuilder().createQuery();
		Root person = criteriaQuery.from(Person.class);
		criteriaQuery.select(person)
				.where(this.getCriteriaBuilder().equal(person.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query = this.getManager().createQuery(criteriaQuery);
		query.setParameter("id", personId);
		Person res = (Person) query.getSingleResult();

		CriteriaUpdate update = this.getCriteriaBuilder().createCriteriaUpdate(House.class);
		Root house = update.from(House.class);
		update.set("person", null);
		update.where(this.getCriteriaBuilder().equal(house.<Long>get("person"), this.getCriteriaBuilder().parameter(Person.class, "person")));
		Query query2 = this.getManager().createQuery(update);
		query2.setParameter("person", res);
		query2.executeUpdate();

		res.getMyHouses().clear();

		for(Person p : res.getMyFriends()){
			Iterator<Person> it = p.getMyFriends().iterator();
			while(it.hasNext()){
				if(it.next().getId() == personId) it.remove();
			}
		}


		res.getMyFriends().clear();
		this.getTx().commit();
		this.getTx().begin();

		CriteriaDelete delete = this.getCriteriaBuilder().createCriteriaDelete(Person.class);
		Root person2 = delete.from(Person.class);
		delete.where(this.getCriteriaBuilder().equal(person2.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query3 = this.getManager().createQuery(delete);
		query3.setParameter("id", personId);
		query3.executeUpdate();
		this.getTx().commit();
	}

	public void deleteHouse(long houseId){
		this.getTx().begin();
		CriteriaQuery criteriaQuery = this.getCriteriaBuilder().createQuery();
		Root house00 = criteriaQuery.from(House.class);
		criteriaQuery.select(house00)
				.where(this.getCriteriaBuilder().equal(house00.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query00 = this.getManager().createQuery(criteriaQuery);
		query00.setParameter("id", houseId);
		House res = (House) query00.getSingleResult();

		CriteriaUpdate update = this.getCriteriaBuilder().createCriteriaUpdate(Heater.class);
		Root heater = update.from(Heater.class);
		update.set("house", null);
		update.where(this.getCriteriaBuilder().equal(heater.<Long>get("house"), this.getCriteriaBuilder().parameter(Person.class, "house")));
		Query query2 = this.getManager().createQuery(update);
		query2.setParameter("house", res);
		query2.executeUpdate();

		CriteriaUpdate update2 = this.getCriteriaBuilder().createCriteriaUpdate(ElectronicDevice.class);
		Root device = update2.from(ElectronicDevice.class);
		update2.set("house", null);
		update2.where(this.getCriteriaBuilder().equal(heater.<Long>get("house"), this.getCriteriaBuilder().parameter(Person.class, "house")));
		Query query3 = this.getManager().createQuery(update2);
		query3.setParameter("house", res);
		query3.executeUpdate();

		this.getTx().commit();

		this.getTx().begin();
		CriteriaDelete delete = this.getCriteriaBuilder().createCriteriaDelete(House.class);
		Root house = delete.from(House.class);
		delete.where(this.getCriteriaBuilder().equal(house.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query = this.getManager().createQuery(delete);
		query.setParameter("id", houseId);
		query.executeUpdate();
		this.getTx().commit();
	}

	public void deleteHeater(long heaterId){
		this.getTx().begin();
		CriteriaDelete delete = this.getCriteriaBuilder().createCriteriaDelete(Heater.class);
		Root heater = delete.from(Heater.class);
		delete.where(this.getCriteriaBuilder().equal(heater.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query = this.getManager().createQuery(delete);
		query.setParameter("id", heaterId);
		query.executeUpdate();
		this.getTx().commit();
	}

	public void deleteDevice(long deviceId){
		this.getTx().begin();
		CriteriaDelete delete = this.getCriteriaBuilder().createCriteriaDelete(ElectronicDevice.class);
		Root device = delete.from(ElectronicDevice.class);
		delete.where(this.getCriteriaBuilder().equal(device.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query = this.getManager().createQuery(delete);
		query.setParameter("id", deviceId);
		query.executeUpdate();
		this.getTx().commit();
	}

	public List<Person> showPersons(){
		CriteriaQuery<Person> query = this.getCriteriaBuilder().createQuery(Person.class);
		Root<Person> from = query.from(Person.class);
		query.select(from);
		TypedQuery<Person> typedQuery = this.getManager().createQuery(query);
		List<Person> res = typedQuery.getResultList();
		return res;
	}

	public List<House> showHouses(){
		List<House> res = this.getManager().createNamedQuery("House.all").getResultList();
		return res;
		/*for (House next : res) {
			System.out.println(next.toString());
		}*/
	}

	public List<Heater> showHeaters(){
		CriteriaQuery<Heater> query = this.getCriteriaBuilder().createQuery(Heater.class);
		Root<Heater> from = query.from(Heater.class);
		query.select(from);
		TypedQuery<Heater> typedQuery = this.getManager().createQuery(query);
		List<Heater> res = typedQuery.getResultList();
		return res;
		/*for (Heater next : res) {
			System.out.println(next.toString());
		}*/
	}

	public List<ElectronicDevice> showDevices(){
		CriteriaQuery<ElectronicDevice> query = this.getCriteriaBuilder().createQuery(ElectronicDevice.class);
		Root<ElectronicDevice> from = query.from(ElectronicDevice.class);
		query.select(from);
		TypedQuery<ElectronicDevice> typedQuery = this.getManager().createQuery(query);
		List<ElectronicDevice> res = typedQuery.getResultList();
		return res;
		/*for (ElectronicDevice next : res) {
			System.out.println(next.toString());
		}*/
	}

	public void houseAddHeater(long houseId, long heaterId){
		boolean b = true;

		CriteriaQuery criteriaQuery = this.getCriteriaBuilder().createQuery();
		Root house = criteriaQuery.from(House.class);
		criteriaQuery.select(house)
				.where(this.getCriteriaBuilder().equal(house.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query = this.getManager().createQuery(criteriaQuery);
		query.setParameter("id", houseId);
		House res = (House) query.getSingleResult();

		CriteriaQuery criteriaQuery2 = this.getCriteriaBuilder().createQuery();
		Root heater = criteriaQuery2.from(Heater.class);
		criteriaQuery2.select(heater)
				.where(this.getCriteriaBuilder().equal(heater.get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query2 = this.getManager().createQuery(criteriaQuery2);
		query2.setParameter("id", heaterId);
		Heater res2 = (Heater) query2.getSingleResult();

		for (Heater he: res.getMyHeaters()) {
			if(he.getId() == res2.getId()){
				b = false;
				break;
			}
		}
		if(b) res2.setHouse(res);
	}

	public void houseAddDevice(long houseId, long deviceId){
		boolean b = true;

		CriteriaQuery criteriaQuery = this.getCriteriaBuilder().createQuery();
		Root house = criteriaQuery.from(House.class);
		criteriaQuery.select(house)
				.where(this.getCriteriaBuilder().equal(house.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query = this.getManager().createQuery(criteriaQuery);
		query.setParameter("id", houseId);
		House res = (House) query.getSingleResult();

		CriteriaQuery criteriaQuery2 = this.getCriteriaBuilder().createQuery();
		Root device = criteriaQuery2.from(ElectronicDevice.class);
		criteriaQuery2.select(device)
				.where(this.getCriteriaBuilder().equal(device.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query2 = this.getManager().createQuery(criteriaQuery2);
		query2.setParameter("id", deviceId);
		ElectronicDevice res2 = (ElectronicDevice) query2.getSingleResult();

		for (ElectronicDevice ed : res.getMyDevices()) {
			if(ed.getId() == res2.getId()){
				b = false;
				break;
			}
		}
		if(b) res2.setHouse(res);
	}

	public void personAddHouse(long personId, long houseId){
		boolean b = true;
		this.getTx().begin();
		CriteriaQuery criteriaQuery = this.getCriteriaBuilder().createQuery();
		Root person = criteriaQuery.from(Person.class);
		criteriaQuery.select(person)
				.where(this.getCriteriaBuilder().equal(person.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query = this.getManager().createQuery(criteriaQuery);
		query.setParameter("id", personId);
		Person res = (Person) query.getSingleResult();

		CriteriaQuery criteriaQuery2 = this.getCriteriaBuilder().createQuery();
		Root house = criteriaQuery2.from(House.class);
		criteriaQuery2.select(house)
				.where(this.getCriteriaBuilder().equal(house.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")));
		Query query2 = this.getManager().createQuery(criteriaQuery2);
		query2.setParameter("id", houseId);
		House res2 = (House) query2.getSingleResult();

		for (House ho : res.getMyHouses()) {
			if(ho.getId() == res2.getId()){
				b = false;
				break;
			}
		}
		if(b) res2.setPerson(res);
		this.getTx().commit();
	}

	public void personAddFriend(long personId, long friendId){
		boolean b1 = true;
		boolean b2 = true;
		this.getTx().begin();
		CriteriaQuery criteriaQuery = this.getCriteriaBuilder().createQuery();
		Root person = criteriaQuery.from(Person.class);
		criteriaQuery.select(person)
				.where(this.getCriteriaBuilder().or(
						this.getCriteriaBuilder().equal(person.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id")),
						this.getCriteriaBuilder().equal(person.<Long>get("id"), this.getCriteriaBuilder().parameter(Long.class, "id2"))
				));
		Query query = this.getManager().createQuery(criteriaQuery);
		query.setParameter("id", personId);
		query.setParameter("id2", friendId);
		List<Person> res = query.getResultList();

		for (Person p1 : res.get(0).getMyFriends()){
			if(p1.getId() == res.get(1).getId()){
				b1 = false;
			}
		}

		for (Person p2 : res.get(1).getMyFriends()){
			if(p2.getId() == res.get(0).getId()){
				b2 = false;
			}
		}
		if(b1 && b2){
			res.get(0).getMyFriends().add(res.get(1));
			res.get(1).getMyFriends().add(res.get(0));
		}
		this.getTx().commit();
	}

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		EntityManagerFactory factory = Persistence.createEntityManagerFactory("mysql");
		EntityManager manager = factory.createEntityManager();
		CriteriaBuilder criteriaBuilder = manager.getCriteriaBuilder();
		EntityTransaction tx = manager.getTransaction();

		JpaTest myJpa = new JpaTest(manager, criteriaBuilder, tx);


		myJpa.getManager().close();
		factory.close();
	}

}
