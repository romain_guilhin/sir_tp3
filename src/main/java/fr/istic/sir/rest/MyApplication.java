package fr.istic.sir.rest;

import org.glassfish.jersey.server.ResourceConfig;
import org.glassfish.jersey.server.ServerProperties;
import org.glassfish.jersey.server.TracingConfig;
import org.glassfish.jersey.server.mvc.MvcFeature;
import org.glassfish.jersey.server.mvc.MvcProperties;

/**
 * Created by Rom on 2/18/2016.
 */
public class MyApplication extends ResourceConfig {

    public MyApplication() {
        // Resources.
        packages(SampleWebService.class.getPackage().getName());

        // MVC.
        register(MvcFeature.class);

        //Template
        property(MvcProperties.TEMPLATE_BASE_PATH, "webapp");

        // Tracing support.
        property(ServerProperties.TRACING, TracingConfig.ON_DEMAND.name());
    }
}
