package fr.istic.sir.rest;

import com.sun.jersey.api.view.Viewable;
import domain.*;
import domain.ElectronicDevice;
import domain.Heater;
import jpa.JpaTest;

import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Iterator;
import java.util.List;

/**
 * Created by Rom on 2/20/2016.
 */
@Path("/house")
public class House {
    private JpaTest myJpa = new JpaTest();
    private List<domain.House> houses = new ArrayList<domain.House>();

    public JpaTest getMyJpa() {
        return myJpa;
    }

    public void setMyJpa(JpaTest myJpa) {
        this.myJpa = myJpa;
    }

    public House() {
        List<domain.House> tmp = this.getMyJpa().showHouses();
        if (tmp.size() > 0)
            for (domain.House h : tmp) {
                houses.add(h);
            }
    }

    @GET
    @Produces("application/json")
    public Collection<domain.House> index() {
        List<domain.House> pp = new ArrayList<domain.House>();
        for (domain.House h : houses) {
            List<ElectronicDevice> ed = new ArrayList<ElectronicDevice>();
            for (ElectronicDevice e : h.getMyDevices()) {
                long l = e.getId();
                ed.add(new ElectronicDevice(l));
            }
            h.setMyDevices(ed);
            List<Heater> eh = new ArrayList<Heater>();
            for (Heater he : h.getMyHeaters()) {
                long l = he.getId();
                eh.add(new Heater(l));
            }
            h.setMyHeaters(eh);
            if(h.getPerson() != null){
                long l = h.getPerson().getId();
                String name = h.getPerson().getName();
                String firstName = h.getPerson().getFirstName();
                h.setPerson(new domain.Person(l, name, firstName));
            }else{
                h.setPerson(new domain.Person("", "", ""));
            }

            pp.add(h);
        }
        return pp;
    }

    @GET
    @Path("new-house")
    public Viewable newHouse() {
        List<domain.Person> persons = myJpa.showPersons();
        List<domain.Heater> heaters = myJpa.showHeaters();
        List<Long> heaterIdList = new ArrayList<Long>();
        for (domain.Heater h : heaters) {
            heaterIdList.add(h.getId());
        }

        List<domain.ElectronicDevice> devices = myJpa.showDevices();
        List<Long> deviceIdList = new ArrayList<Long>();
        for (domain.ElectronicDevice h : devices) {
            deviceIdList.add(h.getId());
        }

        List t = new ArrayList();
        t.add(persons);
        t.add(heaterIdList);
        t.add(deviceIdList);


        return new Viewable("new_house", t);
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("create-house")
    public Response createHouse(@QueryParam("rooms") int rooms, @QueryParam("size") int size, @QueryParam("housePerson") long housePerson, @QueryParam("houseHeater") long houseHeater, @QueryParam("houseDevice") long houseDevice, @Context ServletContext context) {
        List<Long> heaters = new ArrayList<Long>();
        List<Long> devices = new ArrayList<Long>();
        heaters.add(houseHeater);
        devices.add(houseDevice);
        myJpa.createHouse(size, rooms, housePerson, heaters, devices);
        UriBuilder builder = UriBuilder.fromPath(context.getContextPath());
        builder.path("/house");
        return Response.seeOther(builder.build()).build();
    }

    @GET
    @Path("{id}")
    public Viewable houseById(@PathParam("id") long arg0) {
        domain.House h = this.getMyJpa().getHouseById(arg0);
        List<domain.Heater> heaters = myJpa.showHeaters();
        List<domain.ElectronicDevice> devices = myJpa.showDevices();
        List<domain.Person> persons = myJpa.showPersons();
        List<Long> heatersIdList = new ArrayList<Long>();
        List<Long> devicesIdList = new ArrayList<Long>();

        for (domain.Heater he : heaters) {
            heatersIdList.add(he.getId());
        }
        for (domain.ElectronicDevice d : devices) {
            devicesIdList.add(d.getId());
        }
        List t = new ArrayList();
        t.add(h);
        t.add(persons);
        t.add(heatersIdList);
        t.add(devicesIdList);

        return new Viewable("house_info", t);
    }

    @POST
    @Path("update-house")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updateHeater(MultivaluedMap<String, String> form, @Context ServletContext context) {
        int houseId = 0;
        int rooms = 0;
        int size = 0;
        int personId = 0;
        List<Integer> heaters = new ArrayList<Integer>();
        List<Integer> devices = new ArrayList<Integer>();
        Iterator<String> it = form.keySet().iterator();

        while (it.hasNext()) {
            String theKey = (String) it.next();
            if (theKey.startsWith("id")) houseId = Integer.parseInt(form.getFirst(theKey));
            if (theKey.startsWith("size")) size = Integer.parseInt(form.getFirst(theKey));
            if (theKey.startsWith("rooms")) rooms = Integer.parseInt(form.getFirst(theKey));
            if (theKey.startsWith("house-person")) personId = Integer.parseInt(form.getFirst(theKey));
            if (theKey.startsWith("house-heater")) {
                heaters.add(Integer.parseInt(form.getFirst(theKey)));
            }
            if (theKey.startsWith("house-device")) {
                devices.add(Integer.parseInt(form.getFirst(theKey)));
            }
        }
        myJpa.alterHouse(houseId, size, rooms, personId, heaters, devices);
        UriBuilder builder = UriBuilder.fromPath(context.getContextPath());
        builder.path("/house");
        return Response.seeOther(builder.build()).build();
    }

    @POST
    @Path("delete-house/{id}")
    public void deleteHouse(@PathParam("id") long arg0) {
        this.getMyJpa().deleteHouse(arg0);
    }
}
