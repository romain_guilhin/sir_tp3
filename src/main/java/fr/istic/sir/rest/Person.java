package fr.istic.sir.rest;

import com.google.gson.JsonObject;
import com.google.gson.JsonParser;
import com.sun.jersey.api.view.Viewable;
import com.sun.jersey.server.wadl.generators.ObjectFactory;
import domain.*;
import domain.ElectronicDevice;
import domain.Heater;
import domain.House;
import jpa.JpaTest;
import org.hibernate.mapping.Array;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import javax.json.JsonArray;
import javax.json.JsonString;
import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.*;

/**
 * Created by Rom on 2/18/2016.
 */
@Path("/person")
public class Person {
    private JpaTest myJpa = new JpaTest();
    private List<domain.Person> persons = new ArrayList<domain.Person>();
    public JpaTest getMyJpa() {
        return myJpa;
    }
    public void setMyJpa(JpaTest myJpa) {
        this.myJpa = myJpa;
    }

    public Person(){
        List<domain.Person> tmp = this.getMyJpa().showPersons();
        if(tmp.size() > 0)
            for (domain.Person p : tmp) {
                persons.add(p);
            }
    }

    @GET
    @Produces("application/json")
    public Collection<domain.Person> index(){
        List<domain.Person> pp = new ArrayList<domain.Person>();
        for (domain.Person p : persons) {
            List<domain.House> h = new ArrayList<House>();
            for (domain.House hh : p.getMyHouses()) {
                long l = hh.getId();
                h.add(new House(l));
            }
            p.setMyHouses(h);
            List<domain.Person> f = new ArrayList<domain.Person>();
            for (domain.Person ff : p.getMyFriends()) {
                long l = ff.getId();
                String name = ff.getName();
                String firstName = ff.getFirstName();
                f.add(new domain.Person(l, name, firstName));
            }
            p.setMyFriends(f);
            pp.add(p);
        }
        return pp;
    }

    @GET
    @Path("new-person")
    public Viewable newPerson(){
        List<domain.House> houses = myJpa.showHouses();
        List<domain.Person> friends = myJpa.showPersons();
        List<Long> houseIdList = new ArrayList<Long>();

        for (domain.House h : houses) {
            houseIdList.add(h.getId());
        }

        List t = new ArrayList();
        t.add(houseIdList);
        t.add(friends);

        return new Viewable("new_person", t);
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("create-person")
    public Response createPerson(@QueryParam("firstName") String firstName, @QueryParam("name") String name, @QueryParam("mail") String mail, @Context ServletContext context){
        List<Integer> houses = new ArrayList<Integer>();
        List<Integer> friends = new ArrayList<Integer>();
        myJpa.createPerson(name, firstName, mail, houses, friends);
        UriBuilder builder = UriBuilder.fromPath(context.getContextPath());
        builder.path("/person");
        return Response.seeOther(builder.build()).build();
    }

    @GET
    @Path("{id}")
    public Viewable personById(@PathParam("id") long arg0){
        domain.Person p = this.getMyJpa().getPersonById(arg0);
        List<domain.House> houses = myJpa.showHouses();
        List<domain.Person> friends = myJpa.showPersons();
        List<Long> houseIdList = new ArrayList<Long>();

        for (domain.House h : houses) {
            houseIdList.add(h.getId());
        }


        List t = new ArrayList();
        t.add(p);
        t.add(houseIdList);
        t.add(friends);

        return new Viewable("person_info", t);
    }

    @POST
    @Path("update-person")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updatePerson(MultivaluedMap<String, String> form, @Context ServletContext context){
        int id = 0;
        String name = "";
        String firstName = "";
        String mail = "";
        List<Integer> houses = new ArrayList<Integer>();
        List<Integer> friends = new ArrayList<Integer>();
        Iterator<String> it = form.keySet().iterator();

        while(it.hasNext()){
            String theKey = (String)it.next();
            if(theKey.startsWith("id")) id = Integer.parseInt(form.getFirst(theKey));
            if(theKey.startsWith("name")) name = form.getFirst(theKey);
            if(theKey.startsWith("firstName")) firstName = form.getFirst(theKey);
            if(theKey.startsWith("mail")) mail = form.getFirst(theKey);
            if(theKey.startsWith("person-house")){
                houses.add(Integer.parseInt(form.getFirst(theKey)));
            }
            if(theKey.startsWith("person-friend")){
                friends.add(Integer.parseInt(form.getFirst(theKey)));
            }
        }
        myJpa.alterPerson(id, name, firstName, mail, houses, friends);
        UriBuilder builder = UriBuilder.fromPath(context.getContextPath());
        builder.path("/person");
        return Response.seeOther(builder.build()).build();
    }

    @POST
    @Path("delete-user/{id}")
    public void deleteUser(@PathParam("id") long arg0){
        this.getMyJpa().deletePerson(arg0);
    }
}
