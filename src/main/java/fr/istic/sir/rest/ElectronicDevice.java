package fr.istic.sir.rest;

import com.sun.jersey.api.view.Viewable;
import domain.*;
import domain.Heater;
import domain.House;
import jpa.JpaTest;

import javax.servlet.ServletContext;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Rom on 2/20/2016.
 */
@Path("/device")
public class ElectronicDevice {
    private JpaTest myJpa = new JpaTest();
    private List<domain.ElectronicDevice> devices = new ArrayList<domain.ElectronicDevice>();

    public JpaTest getMyJpa() {
        return myJpa;
    }

    public void setMyJpa(JpaTest myJpa) {
        this.myJpa = myJpa;
    }

    public ElectronicDevice(){
        List<domain.ElectronicDevice> tmp = this.getMyJpa().showDevices();
        if(tmp.size() > 0)
            for (domain.ElectronicDevice e : tmp) {
                devices.add(e);
            }
    }

    @GET
    @Produces("application/json")
    public Collection<domain.ElectronicDevice> index(){
        List<domain.ElectronicDevice> pp = new ArrayList<domain.ElectronicDevice>();
        if(!devices.isEmpty()){
            for (domain.ElectronicDevice e : devices) {
                if(e.getHouse() != null){
                    long l = e.getHouse().getId();
                    e.setHouse(new House(l));
                }

                pp.add(e);
            }
        }
        System.out.println(pp);
        return pp;
    }

    @GET
    @Path("new-device")
    public Viewable newDevice(){
        List<domain.House> houses = myJpa.showHouses();
        List<Long> idList = new ArrayList<Long>();
        for (domain.House h : houses) {
            idList.add(h.getId());
        }
        return new Viewable("new_device", idList);
    }

    @GET
    @Path("{id}")
    public Viewable deviceById(@PathParam("id") long arg0){
        domain.ElectronicDevice h = this.getMyJpa().getDeviceById(arg0);
        List<domain.House> houses = myJpa.showHouses();
        List<Long> idList = new ArrayList<Long>();
        for (domain.House ho : houses) {
            idList.add(ho.getId());
        }
        List t = new ArrayList();
        t.add(h);
        t.add(idList);

        return new Viewable("device_info", t);
    }

    @POST
    @Path("update-device")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updateDevice(@FormParam("id") long id, @FormParam("power") int power, @FormParam("device-house-id") long houseId, @Context ServletContext context ){
        HttpServletRequest request;
        HttpServletResponse response;
        myJpa.alterDevice(id, power, houseId);
        UriBuilder builder = UriBuilder.fromPath(context.getContextPath());
        builder.path("/device");
        return Response.seeOther(builder.build()).build();
    }

    @POST
    @Path("delete-device/{id}")
    public void deleteDevice(@PathParam("id") long arg0){
        this.getMyJpa().deleteDevice(arg0);
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("create-device")
    public Response createDevice(@QueryParam("power") int power, @QueryParam("device-house-id") long houseId, @Context ServletContext context ){
        myJpa.createElectronicDevice(power, houseId);
        UriBuilder builder = UriBuilder.fromPath(context.getContextPath());
        builder.path("/device");
        return Response.seeOther(builder.build()).build();
    }
}
