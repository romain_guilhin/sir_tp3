package fr.istic.sir.rest;

import com.sun.jersey.api.view.Viewable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;

/**
 * Created by Rom on 2/21/2016.
 */
@Path("/")
public class Index {
    @GET
    public Viewable index(){
        return new Viewable("index");
    }
}
