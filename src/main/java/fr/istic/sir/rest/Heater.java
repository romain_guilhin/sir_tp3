package fr.istic.sir.rest;

import com.sun.jersey.api.view.Viewable;
import domain.*;
import domain.House;
import domain.Person;
import jpa.JpaTest;
import org.json.JSONArray;
import org.json.JSONObject;

import javax.json.Json;
import javax.json.JsonObject;
import javax.json.JsonObjectBuilder;
import javax.servlet.ServletContext;
import javax.ws.rs.*;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;
import javax.ws.rs.core.UriBuilder;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.Collection;
import java.util.List;

/**
 * Created by Rom on 2/18/2016.
 */
@Path("/heater")
public class Heater {
    private JpaTest myJpa = new JpaTest();
    private List<domain.Heater> heaters = new ArrayList<domain.Heater>();

    public JpaTest getMyJpa() {
        return myJpa;
    }

    public void setMyJpa(JpaTest myJpa) {
        this.myJpa = myJpa;
    }

    public Heater(){
        List<domain.Heater> tmp = this.getMyJpa().showHeaters();
        if(tmp.size() > 0)
            for (domain.Heater h : tmp) {
                heaters.add(h);
            }
    }

    @GET
    @Produces("application/json")
    public Collection<domain.Heater> index(){
        List<domain.Heater> pp = new ArrayList<domain.Heater>();
        if(!heaters.isEmpty()){
            for (domain.Heater p : heaters) {
                if(p.getHouse() != null){
                    long l = p.getHouse().getId();
                    p.setHouse(new House(l));
                }
                pp.add(p);
            }
        }

        System.out.println(pp);
        return pp;
    }

    @GET
    @Path("new-heater")
    public Viewable newHeater(){
        List<domain.House> houses = myJpa.showHouses();
        List<Long> idList = new ArrayList<Long>();
        for (domain.House h : houses) {
            idList.add(h.getId());
        }
        return new Viewable("new_heater", idList);
    }

    @GET
    @Path("{id}")
    public Viewable HeaterById(@PathParam("id") long arg0){
        domain.Heater h = this.getMyJpa().getHeaterById(arg0);
        List<domain.House> houses = myJpa.showHouses();
        List<Long> idList = new ArrayList<Long>();
        for (domain.House ho : houses) {
            idList.add(ho.getId());
        }
        List t = new ArrayList();
        t.add(h);
        t.add(idList);

        return new Viewable("heater_info", t);
    }

    @POST
    @Path("update-heater")
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    public Response updateHeater(@FormParam("id") long id, @FormParam("power") int power, @FormParam("heater-house-id") long houseId, @Context ServletContext context ){
        myJpa.alterHeater(id, power, houseId);
        UriBuilder builder = UriBuilder.fromPath(context.getContextPath());
        builder.path("/heater");
        return Response.seeOther(builder.build()).build();
    }

    @POST
    @Path("delete-heater/{id}")
    public void deleteHeater(@PathParam("id") long arg0){
        this.getMyJpa().deleteHeater(arg0);
    }

    @POST
    @Consumes(MediaType.APPLICATION_FORM_URLENCODED)
    @Path("create-heater")
    public Response createHeater(@QueryParam("power") int power, @QueryParam("heater-house-id") long houseId, @Context ServletContext context ){
        myJpa.createHeater(power, houseId);
        UriBuilder builder = UriBuilder.fromPath(context.getContextPath());
        builder.path("/heater");
        return Response.seeOther(builder.build()).build();
    }
}
