package fr.istic.sir.rest;

import domain.Heater;
import domain.House;
import com.sun.jersey.api.view.Viewable;

import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

@Path("/hello")
public class SampleWebService {

    @GET
    public Viewable sayHello() {

        return new Viewable("index", "FOO");
    }

    @GET
    @Path("/house")
    public Viewable getHouse() {
        System.out.println("fze");
        House h = new House();
        Heater h1 = new Heater();
        h1.setPower(500);
        Heater h2 = new Heater();
        h2.setPower(900);
        h.getMyHeaters().add(h1);
        h.getMyHeaters().add(h2);

        return new Viewable("index", "FOO");    }
}
