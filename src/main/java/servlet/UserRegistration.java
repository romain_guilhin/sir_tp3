package servlet;
import domain.Person;
import jpa.JpaTest;

import java.io.IOException;
import java.io.PrintWriter;
import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.List;

import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

@WebServlet(name="saveUserRegistration",
        urlPatterns={"/user-registration"})
public class UserRegistration extends HttpServlet {

    JpaTest myJpa = new JpaTest();

    public void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        List<Person> persons = myJpa.showPersons();
        ArrayList<Person> pl = new ArrayList<Person>();
        for (Person p : persons) {
            pl.add(p);
        }
        request.setAttribute("content", pl);
        request.getRequestDispatcher("/user-registration-web.jsp").forward(request, response);
    }

    public void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        PrintWriter out = response.getWriter();

        myJpa.createPersonb(
                request.getParameter("name"),
                request.getParameter("firstname"),
                request.getParameter("email")
                );

        List<Person> persons = myJpa.showPersons();
        ArrayList<Person> pl = new ArrayList<Person>();
        for (Person p : persons) {
            pl.add(p);
        }

        request.setAttribute("message", "User saved");
        request.setAttribute("content", pl);

        request.getRequestDispatcher("/user-registration-web.jsp").forward(request, response);


        /*out.println("<HTML>\n<BODY>\n" +
                "<H1>Recapitulatif des informations</H1>\n" +
                "<UL>\n" +
                " <LI>Nom: "
                + request.getParameter("name") + "\n" +
                " <LI>Prenom: "
                + request.getParameter("firstname") + "\n" +
                " <LI>Age: "
                + request.getParameter("age") + "\n" +
                "</UL>\n" +
                "</BODY></HTML>");*/
    }
}