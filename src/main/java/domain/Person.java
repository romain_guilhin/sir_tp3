package domain;
import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rom on 1/8/2016.
 */

@Entity
public class Person {
    private Long id;
    private String name;
    private String firstName;
    private String mail;
    private List<Person> myFriends = new ArrayList<Person>();
    private List<House> myHouses = new ArrayList<House>();

    @OneToMany(mappedBy="person")
    public List<House> getMyHouses() {
        return myHouses;
    }

    public void setMyHouses(List<House> myHouses) {
        this.myHouses = myHouses;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getMail() {
        return mail;
    }

    public void setMail(String mail) {
        this.mail = mail;
    }

    @ManyToMany
    public List<Person> getMyFriends() {
        return myFriends;
    }

    public void setMyFriends(List<Person> myFriends) {
        this.myFriends = myFriends;
    }

    public Person() {
    }
    public Person(long l) {
        this.setId(l);
    }

    public Person(Long id, String name, String firstName, String mail) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
        this.mail = mail;
    }
    public Person(Long id, String name, String firstName) {
        this.id = id;
        this.name = name;
        this.firstName = firstName;
    }
    public Person(String name, String firstName, String mail) {
        this.name = name;
        this.firstName = firstName;
        this.mail = mail;
    }

    @Override
    public String toString() {
        String s  ="[";
        if(this.getMyHouses().size() > 0) {
            int i =0;
            for (House h : this.getMyHouses()) {
                s += "HouseId" + i + ": " + h.getId() + ", ";
                i++;
            }
            s = s.substring(0, s.length() - 2);
        }
        s += "]";
        String s1  ="[";
        if(this.getMyFriends().size() > 0){
            int i =0;
            for (Person p : this.getMyFriends() ) {
                s1 += "FriendId" + i + ": " + p.getId() + ", ";
                i++;
            }
            s1 = s1.substring(0, s1.length()-2);
        }
        s1 += "]";
        return "Person{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", firstName='" + firstName + '\'' +
                ", mail='" + mail + '\'' +
                ", myFriends=" + s1 +
                ", myHouses=" + s +
                '}';
    }
}
