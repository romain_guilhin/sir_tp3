package domain;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.Id;
import javax.persistence.ManyToOne;

/**
 * Created by Rom on 1/8/2016.
 */

@Entity
public class Heater extends IntelligentDevice {
    private Long id;
    private House house;
    private int power;

    @ManyToOne
    public House getHouse() {
        return house;
    }

    public void setHouse(House house) {
        this.house = house;
    }

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getPower() {
        return power;
    }

    public void setPower(int power) {
        this.power = power;
    }

    public Heater(){}

    public Heater(long id){
        this.setId(id);
    }

    public Heater(int power){
        this.setPower(power);
    }
    public Heater(int power, House house){
        this.setPower(power);
        this.setHouse(house);
    }


    @Override
    public String toString() {
        return "Heater{" +
                "id=" + id +
                ", house=" + house +
                ", power=" + power +
                '}';
    }
}
