package domain;

import org.hibernate.annotations.OnDelete;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Rom on 2/16/2016.
 */
@Entity
@NamedQuery(name="House.all",
            query="SELECT h FROM House h")
public class House {
    private Long id;
    private int size;
    private int rooms;
    private Person person;
    private List<Heater> myHeaters = new ArrayList<Heater>();
    private List<ElectronicDevice> myDevices = new ArrayList<ElectronicDevice>();

    @Id
    @GeneratedValue
    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public int getRooms() {
        return rooms;
    }

    public void setRooms(int rooms) {
        this.rooms = rooms;
    }

    @ManyToOne
    public Person getPerson() {
        return person;
    }

    public void setPerson(Person person) {
        this.person = person;
    }

    @OneToMany(mappedBy="house")
    @OnDelete(action = org.hibernate.annotations.OnDeleteAction.CASCADE)
    public List<Heater> getMyHeaters() {
        return myHeaters;
    }

    public void setMyHeaters(List<Heater> myHeaters) {
        this.myHeaters = myHeaters;
    }

    @OneToMany(mappedBy="house")
    @OnDelete(action = org.hibernate.annotations.OnDeleteAction.CASCADE)
    public List<ElectronicDevice> getMyDevices() {
        return myDevices;
    }

    public void setMyDevices(List<ElectronicDevice> myDevices) {
        this.myDevices = myDevices;
    }

    public House(){

    }
    public House(long id){
        this.setId(id);
    }

    public House(int size, int rooms){
        this.setSize(size);
        this.setRooms(rooms);
    }

    public House(int size, int rooms, Person p){
        this.setSize(size);
        this.setRooms(rooms);
        this.setPerson(p);
    }

    @Override
    public String toString() {
        String s  ="[ ";
        if(this.getMyHeaters().size() > 0) {
            int i =0;
            for (Heater h : this.getMyHeaters()) {
                s += "HeaterId" + i + ": " + h.getId() + ", ";
                i++;
            }
            s = s.substring(0, s.length()-2);
        }
        s += "]";

        String s1  ="[ ";
        if(this.getMyDevices().size() > 0) {
            int i =0;
            for (ElectronicDevice h : this.getMyDevices() ) {
                s1 += "DeviceId" + i + ": " + h.getId() + ", ";
                i++;
            }
            s1 = s1.substring(0, s1.length()-2);
        }
        s1 += "]";


        return "House{" +
                "id=" + id +
                ", size=" + size +
                ", rooms=" + rooms +
                ", person=" + person +
                ", myHeaters=" + s +
                ", myDevices=" + s1 +
                '}';
    }
}
