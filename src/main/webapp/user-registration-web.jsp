<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<html lang="en">
<head>
  <meta charset="UTF-8">
  <title>Title</title>
</head>
<body>
  <div id="message">${message}</div>
  <div id="user-form">
    <form method="post" action="/user-registration">
      <label>Name: </label>
      <input type="text" name="name" size=20 />
      <label>FirstName: </label>
      <input type="text" name="firstname" size=20 />
      <label>Email: </label>
      <input type="email" name="email" />
      <input type="submit" value="Send" />
    </form>
  </div>
  <div id="user-list">
    <table>
      <tr>
        <td>Name</td>
        <td>Firstname</td>
        <td>Email</td>
      </tr>
      <c:forEach items="${content}" var="item">
    <tr>
        <td>${item.name}</td>
        <td>${item.firstName}</td>
        <td>${item.mail}</td>
    </tr>
</c:forEach>
    </table>
  </div>
</body>
</html>
