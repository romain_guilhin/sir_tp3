<!DOCTYPE html>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c" %>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn" %>
<html lang="en">
  <head>
    <meta charset="UTF-8">
      <title>Jersey</title>
      <link rel="stylesheet" type="text/css" href="/css/style.css">
        <link rel="stylesheet" type="text/css" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/css/bootstrap.min.css">
          <script src="http://code.jquery.com/jquery-latest.min.js" type="text/javascript"></script>
          <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.6/js/bootstrap.min.js" type="text/javascript"></script>
          <script type="text/javascript" src="/js/main.js"></script>
        </head>
        <body>
          <nav class="navbar navbar-inverse navbar-static-top">
            <div class="container-fluid">
              <div class="navbar-header">
                <a class="navbar-brand" href="/rest">JPA</a>
              </div>
              <ul class="nav navbar-nav">
                <li class="active"><a href="/rest">Home</a></li>
                <li><a href="/rest/person">Manage Persons</a></li>
                <li><a href="/rest/house">Manage Houses</a></li>
                <li><a href="/rest/heater">Manage Heaters</a></li>
                <li><a href="/rest/device">Manage Devices</a></li>
              </ul>
            </div>
          </nav>
