$( document ).ready(function() {

  $('.delete-person').click(function(){
    var row = $(this);
    var id = $(this).attr('id');
    $.ajax({
      type: "post",
      url: "/rest/person/delete-user/" + id,
      success: function(data){
        row.closest('tr').remove();
      }
    });
  });

  $('.delete-heater').click(function(){
    var row = $(this);
    var id = $(this).attr('id');
    $.ajax({
      type: "post",
      url: "/rest/heater/delete-heater/" + id,
      success: function(data){
        row.closest('tr').remove();
      }
    });
  });

  $('.delete-device').click(function(){
    var row = $(this);
    var id = $(this).attr('id');
    $.ajax({
      type: "post",
      url: "/rest/device/delete-device/" + id,
      success: function(data){
        row.closest('tr').remove();
      }
    });
  });

  $('.delete-house').click(function(){
    var row = $(this);
    var id = $(this).attr('id');
    $.ajax({
      type: "post",
      url: "/rest/house/delete-house/" + id,
      success: function(data){
        row.closest('tr').remove();
      }
    });
  });

  $( 'body' ).on('click', '.delete', function() {
    $(this).prev('select').remove();
    $(this).next('.separator').remove();
    $(this).remove();
  });

  $('.add').click(function(){
    if($(this).prev().hasClass('lab')){
      var t = $(this).prev();
      var id = '';
      var index = 0;
      var jsp;
      if($(this).prev().hasClass('heater-pick')){
        id = 'house-heater-id-1';
        jsp = jspHeater;
        }
      if($(this).prev().hasClass('device-pick')){
        id = 'house-device-id-1';
        jsp = jspDevice;
        }
        if($(this).prev().hasClass('house-pick')){
          id = 'person-house-id-1';
          jsp = jspHouse;
          }
      var newSelect = $('<select>');
      newSelect.attr('name', id);
      t.after(newSelect);
      newSelect.after("<span class='delete'><img class='house-heater-delete' src='/img/delete.png'/></span><span class='separator'>|</span>")
      $(jsp).each(function() {
        newSelect.append($("<option>").attr('value',this).text(this));
      });
    }else{
      var arr = [];
      var prevSelect = $(this).prev('.separator').prev('.delete').prev('select');
      prevSelect.find('option').each(function() {
        arr.push(this.value);
      });
      if(arr.length > 1){
        var split = prevSelect.attr('name').split("-")
        var val = parseInt(split[split.length-1]);
        var id = prevSelect.attr('name').substr(0, prevSelect.attr('name').lastIndexOf("-")) + "-";
        val++;
        var newSelect = $('<select>');
        newSelect.attr('name', id + val);
        prevSelect.next('.delete').next('.separator').after(newSelect);
        newSelect.after("<span class='delete'><img class='house-heater-delete' src='/img/delete.png'/></span><span class='separator'>|</span>")
        $(arr).each(function() {
          if(this != prevSelect.val()) newSelect.append($("<option>").attr('value',this).text(this));
        });
      }
    }
  });

  $('.add-person').click(function(){
    if($(this).prev().hasClass('lab')){
      var t = $(this).prev();
      var id = '';
      var jsp;
      if($(this).prev().hasClass('friend-pick')){
        id = 'person-friend-id-1';
        jsp = jspFriend;
        }
      var newSelect = $('<select>');
      newSelect.attr('name', id);
      t.after(newSelect);
      newSelect.after("<span class='delete'><img class='house-heater-delete' src='/img/delete.png'/></span><span class='separator'>|</span>")
      var i = 0;
      $(jsp).each(function() {
        newSelect.append($("<option>").attr('value',this[0]).text(this[1] + " " + this[2]));
        i++;
      });
    }else{
      var arr = [];
      var arrName = [];
      var prevSelect = $(this).prev('.separator').prev('.delete').prev('select');
      prevSelect.find('option').each(function() {
        arr.push(this.value);
        arrName.push(this.text);
      });
      if(arr.length > 1){
        var split = prevSelect.attr('name').split("-")
        var val = parseInt(split[split.length-1]);
        var id = prevSelect.attr('name').substr(0, prevSelect.attr('name').lastIndexOf("-")) + "-";
        val++;
        var newSelect = $('<select>');
        prevSelect.next('.delete').next('.separator').after(newSelect);
        newSelect.attr('name', id + val);
        prevSelect.next('.delete').next('.separator').after(newSelect);
        newSelect.after("<span class='delete'><img class='house-heater-delete' src='/img/delete.png'/></span><span class='separator'>|</span>")
        var i = 0;
        $(arr).each(function() {
          if(this != prevSelect.val()) newSelect.append($("<option>").attr('value',this).text(arrName[i]));
          i++;
        });
      }
    }
  });
});
