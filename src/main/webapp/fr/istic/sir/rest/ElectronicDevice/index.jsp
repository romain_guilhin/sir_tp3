<%@ include file="/templates/header.jsp" %>

<div id="create-device">
  <a href="device/new-device"><button class="create-device btn btn-success">Create new Device</button></a>
</div>
<div id="device-list">
  <h1>Device List </h1>
  <table class = "table table-striped table-hover">
    <tr>
      <td>Id</td>
      <td>Power</td>
      <td>House Id</td>
      <td>Update</td>
      <td>Delete</td>
    </tr>
    <c:forEach items="${it}" var="item">
      <tr>
        <td>${item.id}</td>
        <td>${item.power}</td>
        <td>${item.house.id}</td>
        <td><a href="device/${item.id}" class="btn btn-primary">update</a></td>
        <td><button id="${item.id}" class="delete-device btn btn-danger">Delete</button></td>
      </tr>
    </c:forEach>
  </table>
</div>
</body>
</html>
