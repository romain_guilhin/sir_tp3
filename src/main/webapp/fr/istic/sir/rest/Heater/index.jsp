<%@ include file="/templates/header.jsp" %>

<div id="create-heater">
  <a href="heater/new-heater"><button class="create-heater btn btn-success">Create new Heater</button></a>
</div>
<div id="heater-list">
  <h1>Heater List </h1>
  <table class = "table table-striped table-hover">
    <tr>
      <td>Id</td>
      <td>Power</td>
      <td>House Id</td>
      <td>More</td>
      <td>Update</td>
    </tr>
    <c:forEach items="${it}" var="item">
      <tr>
        <td>${item.id}</td>
        <td>${item.power}</td>
        <td>${item.house.id}</td>
        <td><a href="heater/${item.id}" class="btn btn-primary">update</a></td>
        <td><button id="${item.id}" class="delete-heater btn btn-danger">Delete</button></td>
      </tr>
    </c:forEach>
  </table>
</div>
</body>
</html>
