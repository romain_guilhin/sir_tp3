<%@ include file="/templates/header.jsp" %>

<h1>Add heater</h1>
<div id="new-heater">
  <form method="POST" action="create-heater">
    <label>power: </label>
    <input id="power" type="number" name="power"/><br/>
    <label>House Id: </label>
    <select name="heater-house-id">
        <c:forEach items="${it}" var="item">
        <option>
          ${item}
        </option>
        </c:forEach>
    </select><br />
    <input type="submit" value="Send" class="btn btn-default"/>
  </form>
</div>
</body>
</html>
