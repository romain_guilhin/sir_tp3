<%@ include file="/templates/header.jsp" %>

<div id="heater-info">
  <h1>Heater Info </h1>
  <form method="POST" action="update-heater">
    <label>Id: ${it[0].id}</label>
    <input id="id" type="hidden" name="id" value="${it[0].id}"/><br/>
    <label>power: </label>
    <input id="power" type="number" name="power" value="${it[0].power}"/><br/>
    <label>House Id: </label>
    <select name="heater-house-id">
      <c:forEach items="${it[1]}" var="item">
        <c:choose>
          <c:when test="${item == it[0].house.id}">
            <option selected="selected">
            </c:when>
            <c:otherwise>
              <option>
              </c:otherwise>
            </c:choose>
            ${item}
          </option>
        </c:forEach>
      </select><br />
      <input type="submit" value="Update" class="btn btn-default"/>
    </form>
  </div>
</body>
</html>
