<%@ include file="/templates/header.jsp" %>
<script type="text/javascript">
var jspHeater=${it[2]};
var jspDevice=${it[3]};
</script>
<div id="house-info">
  <h1>house Info </h1>
  <form method="POST" action="update-house">
    <label>Id: ${it[0].id}</label>
    <input id="id" type="hidden" name="id" value="${it[0].id}"/><br/>
    <label>Size: </label>
    <input id="size" type="number" name="size" value="${it[0].size}"/><br/>
    <label>Rooms: </label>
    <input id="rooms" type="number" name="rooms" value="${it[0].rooms}"/><br/>
    <label>Person: </label>
    <select name="house-person-id">
      <c:forEach items="${it[1]}" var="item">
        <c:choose>
          <c:when test="${item.id == it[0].person.id}">
            <option value="${item.id}" selected="selected">
            </c:when>
            <c:otherwise>
              <option value="${item.id}">
              </c:otherwise>
            </c:choose>
            ${item.name} ${item.firstName}
          </option>
        </c:forEach>
      </select><br />
      <label class="lab heater-pick">Heaters: </label>
      <c:set var="count" value="1"/>
      <c:forEach items="${it[0].myHeaters}" var="item">
        <select name="house-heater-id-${count}">
          <c:forEach items="${it[2]}" var="item2">
            <c:choose>
              <c:when test="${item.id == item2}">
                <option selected="selected">
                </c:when>
                <c:otherwise>
                  <option>
                  </c:otherwise>
                </c:choose>
                ${item2}
              </option>
          </c:forEach>
          </select>
          <span class="delete">
            <img class="house-heater-delete" src="/img/delete.png"/>
          </span>
          <span class="separator">
            |
          </span>
          <c:set var="count" value="${count + 1}" scope="page"/>
        </c:forEach>
        <span class="add">
        <img class="house-heater-add" src="/img/add.png"/>
      </span><br>

        <label class="lab device-pick">Devices: </label>
        <c:set var="count" value="1"/>
        <c:forEach items="${it[0].myDevices}" var="item">
          <select name="house-device-id-${count}">
            <c:forEach items="${it[3]}" var="item2">
              <c:choose>
                <c:when test="${item.id == item2}">
                  <option selected="selected">
                  </c:when>
                  <c:otherwise>
                    <option>
                    </c:otherwise>
                  </c:choose>
                  ${item2}
                </option>
            </c:forEach>
            </select>
            <span class="delete">
              <img class="house-heater-delete" src="/img/delete.png"/>
            </span>
            <span class="separator">
              |
            </span>
            <c:set var="count" value="${count + 1}" scope="page"/>
          </c:forEach>
          <span class="add">
          <img class="house-heater-add" src="/img/add.png"/>
        </span><br>
      <input type="submit" value="Update" class="btn btn-default"/>
    </form>
  </div>
</body>
</html>
