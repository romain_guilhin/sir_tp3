<%@ include file="/templates/header.jsp" %>

<div id="create-house">
  <a href="house/new-house"><button class="create-house btn btn-success">Create new House</button></a>
</div>

<div id="house-list">
  <h1>House List </h1>
  <table class = "table table-striped table-hover">
    <tr>
      <td>Id</td>
      <td>Rooms</td>
      <td>Size</td>
      <td>Owner</td>
      <td>Heaters</td>
      <td>Devices</td>
      <td>Update</td>
      <td>Delete</td>
    </tr>
    <c:forEach items="${it}" var="item">
      <tr>
        <td>${item.id}</td>
        <td>${item.rooms}</td>
        <td>${item.size}</td>
        <td>${item.person.name} ${item.person.firstName}</td>
        <td>
          <c:forEach items="${item.myHeaters}" var="item2">
            ${item2.id} /
          </c:forEach>
        </td>
        <td>
          <c:forEach items="${item.myDevices}" var="item3">
            ${item3.id} /
          </c:forEach>
        </td>
        <td><a href="house/${item.id}" class="btn btn-primary">update</a></td>
        <td><button id="${item.id}" class="delete-house btn btn-danger">Delete</button></td>
      </tr>
    </c:forEach>
  </table>
</div>
</body>
</html>
