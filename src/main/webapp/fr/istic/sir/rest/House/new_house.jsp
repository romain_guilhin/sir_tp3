<%@ include file="/templates/header.jsp" %>
<script type="text/javascript">
var jspHeater=${it[1]};
var jspDevice=${it[2]};
</script>
<h1>Add house</h1>
<div id="new-house">
  <form method="POST" action="create-house">
    <label>rooms: </label>
    <input id="rooms" type="number" name="rooms"/><br/>
    <label>size: </label>
    <input id="size" type="number" name="size"/><br/>
    <label>Person: </label>
    <select name="house-person-id">
        <c:forEach items="${it[0]}" var="item">
        <option value="${item.id}">
          ${item.name} ${item.firstName}
        </option>
        </c:forEach>
    </select><br />
    <label class="lab heater-pick">Heaters: </label>
    <select name="house-heater-id-1">
        <c:forEach items="${it[1]}" var="item2">
        <option value="${item2}">
          ${item2}
        </option>
        </c:forEach>
    </select>
    <span class="delete">
      <img class="house-heater-delete" src="/img/delete.png"/>
    </span>
    <span class="separator">
      |
    </span>
    <span class="add">
    <img class="house-heater-add" src="/img/add.png"/>
  </span>
    <br />
    <label class="lab device-pick">Devices: </label>
    <select name="house-device-id-1">
        <c:forEach items="${it[2]}" var="item3">
        <option>
          ${item3}
        </option>
        </c:forEach>
    </select>
    <span class="delete">
      <img class="house-heater-delete" src="/img/delete.png"/>
    </span>
    <span class="separator">
      |
    </span>
    <span class="add">
    <img class="house-heater-add" src="/img/add.png"/>
  </span><br />

    <input type="submit" value="Send" class="btn btn-default"/>
  </form>
</div>
</body>
</html>
