<%@ include file="/templates/header.jsp" %>

<div id="create-person">
  <a href="person/new-person"><button class="create-person btn btn-success">Create new Person</button></a>
</div>

<div id="person-list">
  <h1>Person List </h1>
  <table class = "table table-striped table-hover">
    <tr>
      <td>Id</td>
      <td>Name</td>
      <td>FirstName</td>
      <td>Email</td>
      <td>Houses</td>
      <td>Friends</td>
      <td>Update</td>
      <td>Delete</td>
    </tr>
    <c:forEach items="${it}" var="item">
      <tr>
        <td>${item.id}</td>
        <td>${item.name}</td>
        <td>${item.firstName}</td>
        <td>${item.mail}</td>
        <td>
          <c:forEach items="${item.myHouses}" var="item2">
            ${item2.id} /
          </c:forEach>
        </td>
        <td>
          <c:forEach items="${item.myFriends}" var="item3">
            ${item3.name} ${item3.firstName} /
          </c:forEach>
        </td>

        <td><a href="person/${item.id}" class="btn btn-primary">update</a></td>
        <td><button id="${item.id}" class="delete-person btn btn-danger">Delete</button></td>
      </tr>
    </c:forEach>
  </table>
</div>
</body>
</html>
