<%@ include file="/templates/header.jsp" %>
<script type="text/javascript">
var jspHouse=${it[1]};
var jspFriend =[];
</script>
<c:forEach items="${it[2]}" var="item0">
  <script type="text/javascript">
    jspFriend.push([${item0.id}, "${item0.name}", "${item0.firstName}"]);
  </script>
</c:forEach>
<div id="person-info">
  <h1>Person Info </h1>
  <form method="POST" action="update-person">
    <label>Id: ${it[0].id}</label>
    <input id="id" type="hidden" name="id" value="${it[0].id}"/><br/>
    <label>Name: </label>
    <input id="name" type="text" name="name" value="${it[0].name}"/><br/>
    <label>Firstname: </label>
    <input id="firstName" type="text" name="firstName" value="${it[0].firstName}"/><br/>
    <label>Mail: </label>
    <input id="mail" type="email" name="mail" value="${it[0].mail}"/><br/>
    <label class="lab house-pick">Houses: </label>
    <c:set var="count" value="1"/>
    <c:forEach items="${it[0].myHouses}" var="item">
      <select name="person-house-id-${count}">
        <c:forEach items="${it[1]}" var="item2">
          <c:choose>
            <c:when test="${item.id == item2}">
              <option selected="selected">
              </c:when>
              <c:otherwise>
                <option>
                </c:otherwise>
              </c:choose>
              ${item2}
            </option>
          </c:forEach>
        </select>
        <span class="delete">
          <img class="house-heater-delete" src="/img/delete.png"/>
        </span>
        <span class="separator">
          |
        </span>
        <c:set var="count" value="${count + 1}" scope="page"/>
      </c:forEach>
      <span class="add">
        <img class="house-heater-add" src="/img/add.png"/>
      </span><br>

      <label class="lab friend-pick">Friends: </label>
      <c:set var="count" value="1"/>
      <c:forEach items="${it[0].myFriends}" var="item">
        <select name="person-friends-id-${count}">
            <c:forEach items="${it[2]}" var="item2">
              <c:if test="${item2.id != it[0].id}">
              <c:choose>
                <c:when test="${item.id == item2.id }">
                  <option value="${item2.id}" selected="selected">
                  </c:when>
                  <c:otherwise>
                    <option value="${item2.id}">
                    </c:otherwise>
                  </c:choose>
                  ${item2.name} ${item2.firstName}
                </option>
              </c:if>
              </c:forEach>
          </select>
          <span class="delete">
            <img class="house-heater-delete" src="/img/delete.png"/>
          </span>
          <span class="separator">
            |
          </span>
          <c:set var="count" value="${count + 1}" scope="page"/>
        </c:forEach>
        <span class="add-person">
          <img class="house-heater-add" src="/img/add.png"/>
        </span><br>

        <input type="submit" value="Update" class="btn btn-default"/>
      </form>
    </div>
  </body>
</html>
