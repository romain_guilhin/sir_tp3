<%@ include file="/templates/header.jsp" %>
<script type="text/javascript">
var jspHouse=${it[0]};
var jspFriend =[];
</script>
<c:forEach items="${it[1]}" var="item0">
  <script type="text/javascript">
  jspFriend.push([${item0.id}, "${item0.name}", "${item0.firstName}"]);
  </script>
</c:forEach>
<h1>Add person</h1>
<div id="new-person">
  <form method="POST" action="create-person">
    <div class="form-group">
      <label>Name: </label>
      <input id="name" type="text" name="name"/><br/>
      <label>Firstname: </label>
      <input id="firstName" type="text" name="firstName"/><br/>
      <label>Mail: </label>
      <input id="mail" type="email" name="mail"/><br/>
      <label class="lab house-pick">Houses: </label>
      <select name="person-house-id-1">
        <c:forEach items="${it[0]}" var="item">
          <option value="${item}">
            ${item}
          </option>
        </c:forEach>
      </select>
      <span class="delete">
        <img class="house-heater-delete" src="/img/delete.png"/>
      </span>
      <span class="separator">
        |
      </span>
      <span class="add">
        <img class="house-heater-add" src="/img/add.png"/>
      </span><br>
      <label class="lab friend-pick">Friends: </label>
      <select name="person-friend-id-1">
        <c:forEach items="${it[1]}" var="item2">
          <option value="${item2.id}">
            ${item2.name} ${item2.firstName}
          </option>
        </c:forEach>
      </select>
      <span class="delete">
        <img class="house-heater-delete" src="/img/delete.png"/>
      </span>
      <span class="separator">
        |
      </span>
      <span class="add-person">
        <img class="house-heater-add" src="/img/add.png"/>
      </span><br>
    </div>
    <input type="submit" value="Send" class="btn btn-default"/>
  </form>
</div>
</body>
</html>
